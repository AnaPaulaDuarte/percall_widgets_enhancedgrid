# EnhancedGrid #

Grid to display any data set in ThingWorx.

[EnhancedGrid wiki](https://confluence.percallgroup.com/display/TWX/EnhancedGrid)

## Used in projects ##
* [e-Kit](https://confluence.percallgroup.com/display/ADSAIT)

## Features ##

* Client side pagination
* Filtering global or by column
* Sorting
* Add and updates from server
* Removes rows
* Selection (multiple or not)
* Inline edition with server side update
* Rich headers
* Click events
* Selection
* Custom renderers
* Export as CSV/Excel (computed at client side)
* Custom export formatters
* Column hiding
* Fixed header
* Fixed columns

## Installation ##
* Import extension directly into ThingWorx

## Build ##

* Clone locally the repository
* Import project from eclipse
* Convert project as ThingWorx extension
* Execute build-extension.xml as ANT task



### Who do I talk to? ###

* @rpallares