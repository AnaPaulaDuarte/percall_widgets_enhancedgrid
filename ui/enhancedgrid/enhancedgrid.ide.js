TW.IDE.Widgets.enhancedgrid = function () {
	
	var self = this;
	
	this.widgetIconUrl = function() {
		return  "'../Common/extensions/EnhancedGrid/ui/enhancedgrid/resources/default_widget_icon.png'";
	};

	this.widgetProperties = function () {
		return {
			name: 'EnhancedGrid',
			description: 'Extension that contain a rich table. Inline edit, sorting, filtering, paginate',
			category: ['Common', 'Data'],
			defaultBindingTargetProperty: 'Data',
			supportsAutoResize: true,
			customEditor: 'enhancedgridcustomeditor',
            customEditorMenuText: 'Configure Enhanced Grid Columns',
			properties: {
				Data: {
					description: 'Your data as infotable',
					baseType: 'INFOTABLE',
					isBindingTarget: true,
					isEditable: false,
					warnIfNotBoundAsTarget: true
				},
				PrimaryKey: {
					description: 'Must refer to your primary column key in Data. It\'s not necessary to display that column by defining an entire ColumnDefinition',
					baseType: 'STRING',
					isEditable: true,
					warnIfNotBoundAsTarget: true
				},
				UpdateData: {
					description: 'Push rows here to update current data without having to relaod all the table',
					baseType: 'INFOTABLE',
					isBindingTarget: true
				},
				IsEditable: {
					description: 'Activate the global edition. If true, the EditedDate Infotable is maintained.',
					baseType: 'BOOLEAN',
					isEditable: true,
					defaultValue: false
				},
				UpdateThing: {
					description: 'UpdateThing/Update service will be called for each grid updates',
					baseType: 'THINGNAME',
					isEditable: true
				},
				UpdateService: {
					description: 'UpdateThing/Update service will be called for each grid updates (Service(String row, String? column, JSON oldData, JSON newData) returns integer)',
					baseType: 'STRING',
					isEditable: true
				},
				EditedData: {
					description: 'Data updated throuh grid inline editing features',
					baseType: 'INFOTABLE',
					isBindingSource: true
				},
				ColumnDefinition: {
					description: 'The column definition object DataTables - columns https://datatables.net/reference/option/columns#Type. Default generated if not defined.',
					baseType: 'STRING',
					isEditable: true,
					isBindingSource: true,
					isBindingTarget: true
				},
				DefaultStylingOptions: {
					description: 'Define global table styling options. See https://datatables.net/manual/styling/classes',
					baseType: 'STRING',
					isEditable: true,
					defaultValue: 'compact hover row-border'
				},
				CustomCss: {
					description: 'Additional CSS classes that can be used in ColumnDefinition.className',
					baseType: 'STRING',
					isEditable: true
				},
				EditorColumnParameter: {
					description: 'Stringified JSON object of Editable column parameters. The keys are the column names, and values are the editor parameters. These values override the column definition edit parameter configuration if defined. Useful to define dynamic select options',
					baseType: 'STRING',
					isBindingTarget: true
				},
				ColumnHidden: {
					description: 'Infotable of hidden columns. It datashape should be GenericStringList',
					baseType: 'INFOTABLE',
					isBindingTarget: true
				},
				DeferRender: {
					description: 'Feature control deferred rendering for additional speed of initialisation',
					baseType: 'BOOLEAN',
					isEditable: true,
					defaultValue: true
				},
				DisplayInfo: {
					description: 'Feature control table information display field',
					baseType: 'BOOLEAN',
					isEditable: true,
					defaultValue: true
				},
				Paging: {
					baseType: 'BOOLEAN',
					isEditable: true,
					defaultValue: true
				},
				PageLength: {
					baseType: 'INTEGER',
					isEditable: true,
					defaultValue: 50
				},
				PageLengthChange: {
					baseType: 'BOOLEAN',
					isEditable: true,
					defaultValue: true
				},
				Ordering: {
					baseType: 'BOOLEAN',
					isEditable: true,
					defaultValue: true
				},
				Searching: {
					baseType: 'BOOLEAN',
					isEditable: true,
					defaultValue: true
				},
				IndividualColumnSearching: {
					baseType: 'BOOLEAN',
					isEditable: true,
					defaultValue: false
				},
				ComplexHeader: {
					description: 'Define your table header. Directly define the thead node as described https://datatables.net/examples/basic_init/complex_header.html',
					baseType: 'STRING',
					isEditable: true,
					isBindingTarget: true
				},
				FixedHeader: {
					description: 'When displaying tables with a particularly large amount of data shown on each page, it can be useful to have the table\'s header fixed to the top',
					baseType: 'BOOLEAN',
					isEditable: true,
					defaultValue: true
				},
				FixedColumns: {
					description:'When making use of XAxis scrolling feature, you may with to fix the left column https://datatables.net/extensions/fixedcolumns/',
					baseType: 'STRING',
					isEditable: true,
					defaultValue: 'false'
				},
				EnableSelection: {
					description: 'Activate selection on table',
					baseType: 'BOOLEAN',
					isEditable: true,
					defaultValue: true
				},
				SelectStyle: {
					description: 'Define the select style (single, multi, os)',
					baseType: 'STRING',
					isEditable: true,
					defaultValue: 'os'
				},
				StateSave: {
					description: 'Enable or disable state saving. When enabled aDataTables will store state information such as pagination position, display length, filtering and sorting. \
When the end user reloads the page the table\'s state will be altered to match what they had previously set up.',
					baseType: 'BOOLEAN',
					isEditable: true,
					defaultValue: false
				},
				StateDuration: {
					description: '-1 to save in current session, 0 to save infinetely in browser, the duration in seconds otherwise',
					baseType: 'INTEGER',
					isEditable: true,
					defaultValue: 0
				},
				ClickedColumnName: {
					description: 'Name of the clicked column when ColumnClicked event received',
					baseType: 'STRING',
					isBindingSource: true
				}
			}
		};
	};
	
	this.widgetServices = function() {
		return {
			ResetFilters: {},
			CsvExport: {},
			ExcelExport: {},
			RemoveSelected: {}
		};
	};
	
	this.widgetEvents = function() {
		return {
			RowDblClicked: {},
			CellClicked: {},
			DataEdited: {}
		};
	};
	
	this.afterLoad = function() {};

	this.afterSetProperty = function (name, value) {
		var refreshHtml = false;
		switch (name) {
			default:
				break;
		}
		return refreshHtml;
	};
	
	this.renderHtml = function () {
        return '<div class="widget-content widget-enhancedgrid">\
        	<table height="100%" width="100%">\
        		<tr>\
        			<td valign="middle" align="center"><span>Enhanced grid</span></td>\
        		</tr>\
        	</table>\
        </div>';
    };
    
    this.validate = function() {
    	var result = [];
    	
    	if(this.isPropertyBoundAsTarget('ColumnHidden')) {
    		var metadata = this.getInfotableMetadataForProperty('ColumnHidden');
    		if(!metadata || !metadata.item) {
    			result.push({severity: 'error', message: 'ColumnHidden for {target-id} is not bound to a datashape containing the item field. Use the GenericStringList data shape'});
    		}
    	}
    	
    	return result;
    };

};