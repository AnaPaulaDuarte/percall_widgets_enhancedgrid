TW.IDE.Dialogs.enhancedgridcustomeditor = function () {
	var self = this;
	
	this.title = "Enhanced Grid columns";
	this.currentColDefinitions = {};
    this.gridLibrary = new TW.EnhancedGridLibrary();
    this.defaultRenderers = this.gridLibrary.getDefaultRenderers();
    this.gridConfigDOM = null;
    
    this.renderDialogHtml = function (widgetObj) {
    	var properties = widgetObj.properties;
        var resultingShape = widgetObj.getInfotableMetadataForProperty('Data');
        var loadedColDefinitions = [];
        var defaultChecked = 'checked="checked"';
        
        if(!!properties.ColumnDefinition) {
        	loadedColDefinitions = JSON.parse(properties.ColumnDefinition, this.gridLibrary.parseReviver);
        	
        	loadedColDefinitions.forEach(function(item, index) {
        		self.currentColDefinitions[item.data] = item;
        	});
        	
        	defaultChecked = '';
        }
        
        
        var cId, cDef;
        
        var htmlFieldList = '<ul class="nav nav-list well well-small" id="grid-config-list-ul"><div class="nav-header">DRAG TO REORDER</div>';
        
        if(!resultingShape) {
        	htmlFieldList += '<div>[Grid must be bound to data]</div>';
        }
        
        else {
        	 var generatedColDefinitions = this.gridLibrary.dataShapeToColumnFormat(resultingShape);
        	
        	 //load existing data
        	 
        	 for(cId in this.currentColDefinitions) {
        		 this.currentColDefinitions[cId].selected = true;
        		 cDef = this.currentColDefinitions[cId];
				
        		 htmlFieldList += '<li>' +
        		 	'<a>' +
        		 		'<div class="re-order"></div>' +
        		 		'<div class="column-name" title="' + cDef.data + '">' + cDef.data + '</div>' +
        		 		'</a>' +
        		 	'<div class="checkbox"><input type="checkbox" checked="checked" class="visible"/></div>' +
        		 '</li>';
        	 }
	        
        	 //load leading data
        	 for(cId in generatedColDefinitions) {
        		 cDef = generatedColDefinitions[cId];
        		 cDef.selected = defaultChecked!=='';
	        	
        		 //add only if not added before
        		 if(!this.currentColDefinitions[cDef.data]) {
        			 this.currentColDefinitions[cDef.data] = cDef;
				
        			 htmlFieldList += '<li>' +
        			 	'<a>' +
        			 		'<div class="re-order"></div>' +
        			 		'<div class="column-name" title="' + cDef.data + '">' + cDef.data + '</div>' +
        			 	'</a>' +
        			 	'<div class="checkbox"><input type="checkbox" ' + defaultChecked + ' class="visible"/></div>' +
        			 '</li>';
        		 }
        	}
        }
        
        htmlFieldList += '</ul>';
        
        var html =
	        '<div id="grid-config-container">'
		        + '<div id="grid-config-list">'
					+ '<div class="grid-config-list-header btn-group">'
				        + '<button class="btn btn-mini config-list-cmd cmd-deselect-all" cmd="unselect-all">Deselect All</button>'
				        + '<button class="btn btn-mini config-list-cmd cmd-select-all" cmd="select-all">Select All</button>'
			        + '</div>'
			        + htmlFieldList
		        + '</div>'
		        + '<div id="grid-config">'
			        + '<div class="configuration-column">'
                        + '<div class="grid-config-tab-header">'
                            + '<h4 class="grid-column-name" id="field-name"></h4>'
                            + '<div class="tab-name selected" tab-name="column-info">'
                                + '<span>Column format</span>'
                            + '</div>'
                            + '<div class="tab-name" tab-name="column-renderer">'
                                + '<span>Column renderer</span>'
                            + '</div>'
                            + '<div class="tab-name" tab-name="column-editor">'
	                            + '<span>Column editor</span>'
	                        + '</div>'
                        + '</div>'
                        + '<div class="grid-config-tabs">'
                            + '<div class="column-info tab">'
                                + '<table class="table table-striped table-bordered">'
                                	+ '<tr>'
                                		+ '<td class="grid-config-label"><label>Column title</label></td>'
                                		+ '<td><input name="column-title" type="text"></input></td>'
                                	+ '</tr>'
                                	+ '<tr>'
	                            		+ '<td class="grid-config-label"><label>Default content</label></td>'
	                            		+ '<td><input name="column-default" type="text"></input></td>'
	                            	+ '</tr>'
	                            	+ '<tr>'
	                            		+ '<td class="grid-config-label"><label>Column width</label></td>'
	                            		+ '<td><input name="column-width" type="text"></input></td>'
	                            	+ '</tr>'
	                            	+ '<tr>'
	                            		+ '<td class="grid-config-label"><label>Class names</label></td>'
	                            		+ '<td><input name="column-classname" type="text"></input></td>'
	                            	+ '</tr>'
	                            	+ '<tr>'
	                            		+ '<td class="grid-config-label"><label>Is orderable</label></td>'
	                            		+ '<td><input name="column-orderable" type="checkbox"></input></td>'
	                            	+ '</tr>'
	                            	+ '<tr>'
	                            		+ '<td class="grid-config-label"><label>Is searchable</label></td>'
	                            		+ '<td><input name="column-searchable" type="checkbox"></input></td>'
	                            	+ '</tr>'
	                            	+ '<tr>'
	                            		+ '<td class="grid-config-label"><label>Is search select</label></td>'
	                            		+ '<td><input name="column-searchselect" type="checkbox"></input></td>'
	                            	+ '</tr>'
		                            + '<tr>'
	                            		+ '<td class="grid-config-label"><label>Datatable type</label></td>'
	                            		+ '<td><select name="column-type">'
                            				+ '<option value="string">string</option>'
	                            			+ '<option value="num">num</option>'
	                            			+ '<option value="date">date</option>'
	                            			+ '<option value="num-fmt">num-fmt</option>'
	                            			+ '<option value="html-num">html-num</option>'
	                            			+ '<option value="html-num-fmt">html-num-fmt</option>'
	                            			+ '<option value="html">html</option>'
	                            			+ '</select></td>'
	                            	+ '</tr>'
	                            	+ '<tr>'
	                            		+ '<td class="grid-config-label"><label>Is clickable</label></td>'
	                            		+ '<td><input name="column-clickable" type="checkbox"></input></td>'
	                            	+ '</tr>'
	                            + '</table>'
                            + '</div>'
                            + '<div class="column-renderer tab" style="display: none;">'
                            	+ '<label>Renderer</label>'
                            	+ '<select name="column-render">'
                            		+ '<option value="text">Default</option>'
                            		+ '<option value="isodate">ISO Date</option>'
                            		+ '<option value="isodatetime">ISO Date and time</option>'
                            		+ '<option value="boolean">Boolean checkbox</option>'
                            		+ '<option value="hyperlink">Hyperlink</option>'
                            		+ '<option value="location">Location</option>'
                            		+ '<option value="ellipsis">Ellipsis</option>'
                            		+ '<option value="imagelink">Imagelink</option>'
                            		+ '<option value="grouplink">Grouplink</option>'
                            		+ '<option value="mashuplink">Mashuplink</option>'
                            		+ '<option value="menulink">Menulink</option>'
                            		+ '<option value="thinglink">Thinglink</option>'
                            		+ '<option value="userlink">Userlink</option>'
                            		+ '<option value="array">Array</option>'
                            		+ '<option value="json">JSON</option>'
                            		+ '<option value="custom">Custom</option>'
                            	+ '</select>'
                            	+ '<div name="custom-render-div">'
                            		+ '<label>Custom renderer function</label>'
                            		+ '<textarea name="column-custom-render" cols="2" rows="10" style="width:100%;"/>'
                            		+ '<span class="disabled-explanation">Fill the custom render value as described in <a target="_blank" href="https://datatables.net/reference/option/columns.render">Column.render</a> section</span>'
                            	+ '</div>'
                            	+ '<div name="custom-export-div">'
	                        		+ '<label>Custom export function</label>'
	                        		+ '<textarea name="column-custom-export" cols="2" rows="10" style="width:100%;"/>'
	                        		+ '<span class="disabled-explanation">Fill the custom export format function as described in <a target="_blank" href="https://datatables.net/extensions/buttons/examples/html5/outputFormat-function.html">Output format</a> section. Your function will be called only for your column if visible</span>'
	                        	+ '</div>'
	                        + '</div>'
                        	+ '<div class="column-editor tab" style="display: none;">'
                            	+ '<tr>'
                            		+ '<td class="grid-config-label"><label>Is editable</label></td>'
                            		+ '<td><input name="column-editable" type="checkbox"></input></td>'
                            	+ '</tr>'
                            	+ '<div name="column-editor-options">'
	                            	+ '<label>Edit type</label>'
	                            	+ '<select name="column-edit-type">'
	                            		+ '<option value="text">Text</option>'
	                            		+ '<option value="textarea">Text area</option>'
	                            		+ '<option value="select">Select</option>'
	                            		+ '<option value="datepicker">Date picker</option>'
	                            		+ '<option value="datetimepicker">Date time picker</option>'
	                            		+ '<option value="checkbox">Checkbox</option>'
	                            	+ '</select>'
	                            	+ '<label>Edit Data</label>'
	                            	+ '<input name="column-edit-data" type="text"></input>'
	                            	+ '<div name="custom-edit-div">'
		                        		+ '<label>Editor parameters</label>'
		                        		+ '<textarea name="column-edit-param" cols="2" rows="10" style="width:100%;"/>'
		                        		+ '<span class="disabled-explanation">Fill as described in documentation</span>'
		                        	+ '</div>'
		                        + '</div>'
                        	+ '</div>'
                        + '</div>'
                    + '</div>'
		        + '</div>'
				+ '<div class="clear"></div>'
	        + '</div>';


        return html;
    };
    
    this.afterRender = function (domElementId) {
    	this.jqElementId = domElementId;
        this.jqElement = $('#' + domElementId);
        
        //sets up the tabs for the column info & the renderer/state
        this.gridConfigDOM = this.jqElement.find('#grid-config-container');
        this.gridConfigListDOM = this.jqElement.find('#grid-config-list');
        
        //panel navigation
		this.gridConfigDOM.on('click','.grid-config-tab-header .tab-name',function(e) {
		    e.stopPropagation();
		    self.gridConfigDOM.find('.grid-config-tab-header .tab-name').removeClass('selected');
		    var selTab = $(e.target).closest('.tab-name');
		    selTab.addClass('selected');
		    self.gridConfigDOM.find('.grid-config-tabs .tab').hide();
		    self.gridConfigDOM.find('.grid-config-tabs').find('.tab.' + selTab.attr('tab-name')).show().addClass('selected');
		});
		
		//list sortable
		if (this.gridConfigListDOM.find('ul#grid-config-list-ul li').length === 0) {
            this.jqElement.find('#grid-config').hide();
        }
		else {
			// make the list of fields draggable and make it so the user can't select text within the names
			this.gridConfigListDOM.find('ul#grid-config-list-ul').sortable().disableSelection();
			this.gridConfigListDOM.find('ul#grid-config-list-ul li').click(this._onElementClickHandler);
			this.gridConfigListDOM.find('ul#grid-config-list-ul li input').change(this._onElementCheckboxChangeHandler);
            this.jqElement.find('.config-list-cmd').click(this._onconfigListSelectHandler);
		}
		
		//custom render sub panel
		var customPanel = self.gridConfigDOM.find("div[name='custom-render-div']");
		this.gridConfigDOM.find("select[name='column-render']").change(function() {
			if(this.value==='custom') {
				customPanel.show();
			}
			else {
				customPanel.hide();
			}
		});
		
		//add input variables
		this.inputTitle = this.gridConfigDOM.find("input[name='column-title']");
		this.inputDefault = this.gridConfigDOM.find("input[name='column-default']");
		this.inputWidth = this.gridConfigDOM.find("input[name='column-width']");
		this.inputClassName = this.gridConfigDOM.find("input[name='column-classname']");
		this.inputOrderable = this.gridConfigDOM.find("input[name='column-orderable']");
		this.inputSearchable = this.gridConfigDOM.find("input[name='column-searchable']");
		this.inputSearchSelect = this.gridConfigDOM.find("input[name='column-searchselect']");
		this.inputType = this.gridConfigDOM.find("select[name='column-type']");
		this.inputClickable = this.gridConfigDOM.find("input[name='column-clickable']");
		
		this.inputRender = this.gridConfigDOM.find("select[name='column-render']");
		this.inputRenderCustom = this.gridConfigDOM.find("textarea[name='column-custom-render']");
		this.inputExportFormater = this.gridConfigDOM.find("textarea[name='column-custom-export']");

		this.inputEditable = this.gridConfigDOM.find("input[name='column-editable']");
		this.inputEditType = this.gridConfigDOM.find("select[name='column-edit-type']");
		this.inputEditData = this.gridConfigDOM.find("input[name='column-edit-data']");
		this.inputEditParam = this.gridConfigDOM.find("textarea[name='column-edit-param']");
		
		this.inputEditable.change(function() {
			if(self.inputEditable.prop('checked')) {
				self.gridConfigDOM.find("div[name='column-editor-options']").show();
			}
			else {
				self.gridConfigDOM.find("div[name='column-editor-options']").hide();
			}
		});
		
		// select the first field in the list
        this.itemSelected(this.gridConfigListDOM.find('ul#grid-config-list-ul li:first'));
    };
    
    /**
	 * user is ready to leave the dialog ... validate and save properties in the appropriate property in the property bag
	 */
    this.updateProperties = function (widgetObj) {
    	console.log('update properties');
    	var $activeLi = this.gridConfigListDOM.find('ul#grid-config-list-ul li.active');
    	
    	// call itemSelectionLeaving() on current target and validate that it's ok before we save
    	if (!this.itemSelectionLeaving($activeLi)) {
            return false;
        }
    	
    	//build column format
    	var columnFormat = [];
    	
    	var $lis = this.gridConfigListDOM.find('ul#grid-config-list-ul li');
    	$lis.each(function() {
    		var $li = $(this);
			
    		if(self.fieldChecked($li)) {
    			var cId = self.getColumnIdFromLi($li);
    			var cDef = self.currentColDefinitions[cId];
    			delete cDef.selected;
    			columnFormat.push(cDef);
    		}
    	});
    	
        widgetObj.setProperty('ColumnDefinition', JSON.stringify(columnFormat, this.gridLibrary.stringifyReplacer));
    	
    	this.jqElement.off();
    	return true;
    };
    
    /**
     * save whatever is in the UI for this field
     */
    this.itemSelectionLeaving = function ($liTarget) {
    	if (!!$liTarget && $liTarget.length>0) {
	    	var cId = this.getColumnIdFromLi($liTarget);
			var cDef = this.currentColDefinitions[cId];
		
    		cDef.selected = this.fieldChecked($liTarget);
    		cDef.title = this.inputTitle.val();
    		cDef.defaultContent = this.inputDefault.val();
    		cDef.width = !!this.inputWidth.val().trim() ? this.inputWidth.val().trim() : undefined;
    		cDef.className = this.inputClassName.val();
    		cDef.orderable = this.inputOrderable.prop('checked');
    		cDef.searchable = this.inputSearchable.prop('checked');
    		cDef.issearchselect = this.inputSearchSelect.prop('checked');
    		cDef.type = this.inputType.val();
    		cDef.clickable = this.inputClickable.prop('checked');
    		
    		if(cDef.title.length===0) {
    			alert('Title cannot be empty');
    			return false;
    		}
    		
    		// parse render
    		try {
    			cDef.render = this.getRenderValue();
    		}
    		catch(e) {
    			alert('Custom render cannot be parsed. Please verify your custom render definition. ' + e.message);
    			return false;
    		}
    		
    		// parse export formatter
    		try {
    			cDef.exportFormatter = this.getExportFormaterValue();
    		}
    		catch(e) {
    			alert('Custom export formatter cannot be parsed. Please verify your formatter definition. ' + e.message);
    			return false;
    		}
    		
    		//parse edit
    		cDef.editable = this.inputEditable.prop('checked');
    		cDef.editType = this.inputEditType.val();
    		cDef.editData = this.inputEditData.val();
    		try {
    			cDef.editParam = !!this.inputEditParam.val() ? JSON.parse(this.inputEditParam.val()) : null;
    		}
    		catch(e) {
    			alert('Edit parameters are not valid. Please verify your edit parameter definition. ' + e.message);
    			return false;
    		}
    		
    		this.currentColDefinitions[cId] = cDef;
    	}
    	return true;
    };
    
    /**
     * handle selecting an item - populate the dialog on the right
     */
    this.itemSelected = function ($liTarget) {
		$liTarget.addClass('active');
		var cId = this.getColumnIdFromLi($liTarget);
		var cDef = this.currentColDefinitions[cId];
		
		this.inputTitle.val(cDef.title);
		this.inputDefault.val(cDef.defaultContent);
		this.inputWidth.val(!!cDef.width ? cDef.width : '');
		this.inputClassName.val(cDef.className);
		this.inputOrderable.prop('checked', cDef.orderable);
		this.inputSearchable.prop('checked', cDef.searchable);
		this.inputSearchSelect.prop('checked', cDef.issearchselect);
		this.inputType.val(cDef.type);
		this.inputClickable.prop('checked', cDef.clickable);
				
		this.setRenderValue(cDef);
		this.setExportFormatterValue(cDef);
		
		this.inputEditable.prop('checked', cDef.editable).trigger('change');
		this.setEditTypeValue(cDef);
		this.inputEditData.val(!!cDef.editData ? cDef.editData : '');
		this.setEditParamValue(cDef);
    };
    
    /**
     * helper to find the <li> from any html element inside an li
     * @return [Jquery object]
     */
    this.closestLi = function ($element) {
        return $element.closest('li');
    };
    
    /**
     * Helper method to see if this field is checked or not
     * @return bool
     */
    this.fieldChecked = function ($li) {
        return $li.find('input').is(":checked");
    };
    
    /**
     * Helper method to retrieve li column name
     * @return string the column id
     */
    this.getColumnIdFromLi = function($li) {
    	return $li.find('div.column-name').text();
    };
    
    /**
     * Helper to define render value
     */
    this.setRenderValue = function(cDef) {
    	switch(typeof cDef.render) {
			case "string":
				this.inputRender.val('custom');
				this.inputRenderCustom.val(cDef.render);
				break;
				
			case "object":
				this.inputRender.val('custom');
				this.inputRenderCustom.val(JSON.stringify(cDef.render, null, 2));
				break;
				
			case "function":
				if(!!this.defaultRenderers[cDef.render.name]) {
					this.inputRender.val(cDef.render.name);
					this.inputRenderCustom.val('');
				}
				else {
					this.inputRender.val('custom');
					this.inputRenderCustom.val(String(cDef.render));
				}
				break;
				
			default:
				this.inputRender.val('text');
				this.inputRenderCustom.val('');
				break;
		}
    	
    	this.inputRender.trigger('change');
    };
    
    /**
     * Helper to define export formatter value
     */
    this.setExportFormatterValue = function(cDef) {
    	if(typeof cDef.exportFormatter === 'function') {
    		this.inputExportFormater.val(String(cDef.exportFormatter));
    	}
    	else {
    		this.inputExportFormater.val('');
    	}
    };
    
    this.setEditParamValue = function(cDef) {
    	if(!!cDef.editParam) {
    		this.inputEditParam.val(JSON.stringify(cDef.editParam));
    	}
    	else {
    		this.inputEditParam.val('');
    	}
    };
    
    this.setEditTypeValue = function(cDef) {
    	if(!!cDef.editType) {
    		this.inputEditType.val(cDef.editType);
    	}
    	else if (!!cDef.baseType){
    		this.inputEditType.val(this.gridLibrary.baseType2EditType(cDef.baseType2EditType));
    	}
    	else {
    		this.inputEditType.val('text');
    	}
    };
    
	/**
	 * Helper method to get render value
	 * Can raise exception from eval function
	 * @return [function] render function or undefined
	 */
    this.getRenderValue = function() {
    	var renderString = this.inputRender.val();
    	var defaultRender = this.defaultRenderers[renderString];
    	if(!!defaultRender) {
    		return defaultRender;
    	}
    	else {
    		return this.gridLibrary.parseFunctionFromString(this.inputRenderCustom.val());
    	}
    };
    
    /**
     * Helper method to get custom export formatter
     * Can raise exception from eval function
     * @return [function] formatter function or undefined
     */
    this.getExportFormaterValue = function() {
    	return this.gridLibrary.parseFunctionFromString(this.inputExportFormater.val());
    };
    
    
    /*
     * JQuery handlers
     */
    
    this._onElementClickHandler = function(e) {
    	// we get a click here for any html element inside the <li> ... find the appropriate li
        var $liTarget = $(e.target);
        if (e.target.localName !== 'li') {
        	$liTarget = self.closestLi($liTarget);
        }
        
        // see if this one is already selected
        if (!$liTarget.hasClass('active')) {
        	
        	var $activeLi = self.gridConfigListDOM.find('ul#grid-config-list-ul li.active');
        	
            if (self.itemSelectionLeaving($activeLi)) {
            	self.gridConfigListDOM.find('ul#grid-config-list-ul li').removeClass('active');
                self.itemSelected($liTarget);
            }
        }
    };
    
    this._onElementCheckboxChangeHandler = function(e) {
    	var $li = self.closestLi($(e.target));
    	var cId = self.getColumnIdFromLi($li);
    	
		self.currentColDefinitions[cId].selected = self.fieldChecked($li);
    };
    
    this._onconfigListSelectHandler = function(e) {
    	var btn = $(e.target).closest('.config-list-cmd');
        var liInitiallySelected = self.gridConfigListDOM.find('ul#grid-config-list-ul li.active');
        var cmd = btn.attr('cmd');
        var toState = cmd==='select-all';
        
        _.each(self.gridConfigListDOM.find('ul#grid-config-list-ul li'), function(li) {
        	var $li = $(li);
        	if(self.fieldChecked($li) !== toState) {
        		$li.find('input').prop('checked', toState);
        	}
        });
    };
    
};