/*
 * Datepicker for Jeditable
 *
 * Copyright (c) 2011 Piotr 'Qertoip' Włodarek
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Depends on jQuery UI Datepicker
 *
 * Project home:
 *   http://github.com/qertoip/jeditable-datepicker
 *
 */

// add :focus selector
jQuery.expr[':'].focus = function( elem ) {
  return elem === document.activeElement && ( elem.type || elem.href );
};

$.editable.addInputType( 'datepicker', {

    /* create input element */
    element: function(settings, original) {
    	var $input = $('<input />');
    	$(this).append($input);
    	return $input;
    },
    
    plugin: function(settings, original) {
    	var form = this;
    	var input = form.find('input');
    	
    	settings.onblur = 'nothing';
    	
    	input.datepicker({
    		changeYear: true,
    		showButtonPanel: true,
    		closeText: 'Clear',
    		beforeShow: function(input, picker) {
    			setTimeout(function() {
	    			var btn = picker.dpDiv.find('.ui-datepicker-buttonpane .ui-datepicker-close');
	    			btn.on('click', function() {
	    				$(input).val('').submit();
	    			});
    			}, 10);
    		},
    		onSelect: function(dateText, picker) {
    			input.blur();
    			input.datepicker('hide');
    			form.submit();
    		},
    		onClose: function(dateText,picker) {
    			setTimeout(function() {
    				if(!input.is(':focus')) {
    					original.reset(form);
    				}
    				else {
    					form.submit();
    				}
    			}, 150);
    		}
    	});
    }
    
});
//# sourceURL=jquery.jeditable.datepicker.js