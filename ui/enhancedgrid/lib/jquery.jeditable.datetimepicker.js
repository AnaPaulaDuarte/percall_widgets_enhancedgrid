/*
 *
 */

// add :focus selector
jQuery.expr[':'].focus = function( elem ) {
  return elem === document.activeElement && ( elem.type || elem.href );
};

$.editable.addInputType( 'datetimepicker', {

    /* create input element */
    element: function( settings, original ) {
    	var $input = $('<input />');
    	$(this).append($input);
    	return $input;
    },
    
    plugin: function(settings, original) {
    	var form = this;
    	var input = form.find('input');
    	
    	input.datetimepicker({
    		changeYear: true,
    		showSecond: true,
    		timeFormat: 'hh:mm:ss',
            showButtonPanel: true,
            closeText: 'Clear',
            beforeShow: function(input, picker) {
    			setTimeout(function() {
	    			var btn = picker.dpDiv.find('.ui-datepicker-buttonpane .ui-datepicker-close');
	    			btn.on('click', function() {
	    				$(input).val('').submit();
	    			});
    			}, 10);
    		},
    		onSelect: function(dateText, picker) {
    			var dateEntered = new Date(picker.selectedYear, picker.selectedMonth, picker.selectedDay,
						!!picker.selectedHour ? picker.selectedHour : 0,
						!!picker.selectedMinute ? picker.selectedMinute : 0,
						!!picker.selectedSecond ? picker.selectedSecond : 0);
    			
    			input.val(moment(dateEntered).format('YYYY-MM-DD HH:mm:ss'));
    			
    			input.blur();
    			input.datetimepicker('hide');
    			form.submit(dateEntered);
    		},
    		onClose: function(dateText,picker) {
    			original.reset(form);
    		}
    	});
    }
    
});
//# sourceURL=jquery.jeditable.datetimepicker.js