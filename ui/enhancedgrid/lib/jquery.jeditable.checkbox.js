
// add 'Checkbox' input type to Jeditable.
$.editable.addInputType("checkbox", {
	/* This uses default hidden input field. No need for element() function. */
	
	plugin: function(settings, original) {
		var form = this;
		var hiddenInput = $(original).find("input[type='hidden']");
		var originaleValue = hiddenInput.val();
		var originalCheckbox;
		var value = false;
		try {
			originalCheckbox = $(originaleValue);
		}
		catch(e) {}
		
		/* test if render is boolean render */
		if(!!originalCheckbox && originalCheckbox.length>0) {
			originalCheckbox.prop('checked', !originalCheckbox.prop('checked'));
			value = originalCheckbox.prop('checked');
		}
		/* other cases of original data */
		else {
			try {
				var parsed = JSON.parse(originaleValue);
				if(typeof parsed!=='boolean') {
					/* may be there is quotes or double quotes on value */
					originaleValue = originaleValue.replace(/["']/g, '');
					parsed = JSON.parse(originaleValue);
				}
				
				value = (typeof parsed==='boolean') ? !parsed : !originaleValue;
			}
			catch(e) {
				value = !originaleValue;
			}
		}

		$('input', this).val(value.toString());
		
		/* submit automatically */
		setTimeout(function() {
			form.submit();
		}, 10);
	}
});
//# sourceURL=jquery.jeditable.checkbox.js
