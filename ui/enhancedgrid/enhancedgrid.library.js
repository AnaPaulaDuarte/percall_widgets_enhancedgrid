
TW.EnhancedGridLibrary = function() {
	
	var self = this;
	var defaultRenderers = null;
	
	/**
	 * @return the default renderers object
	 */
	this.getDefaultRenderers = function() {
		if(defaultRenderers===null) {
			defaultRenderers = {
				text: function text(data, type, full, meta){
					return (type==='display' && 'string'===typeof data) ? _.escape(data) : data;
				},
				hyperlink: function hyperlink(data, type, full, meta) {
					var ellipsed = data.length>30 ? data.substr(0,27)+'...' : data;
					return type==='display' ? '<a target="_blank" href="'+data+'">' + ellipsed + '</a>' : data;
				},
				imagelink: function imagelink(data, type, full, meta) {
					return type==='display' ? '<img src="'+data+'"/>' : data;
				},
				grouplink: function grouplink(data, type, full, meta) {
					return type==='display' ? '<a about="_blank" href="/Thingworx/Groups/'+data+'">'+ data+'</a>' : data;
				},
				mashuplink: function mashuplink(data, type, full, meta) {
					return type==='display' && !!data ? '<a about="_blank" href="/Thingworx/Mashups/'+data+'">'+ data+'</a>' : data;
				},
				menulink: function menulink(data, type, full, meta) {
					return type==='display' && !!data ? '<a about="_blank" href="/Thingworx/Menus/'+data+'">'+ data+'</a>' : data;
				},
				thinglink: function thinglink(data, type, full, meta) {
					return type==='display' && !!data ? '<a about="_blank" href="/Thingworx/Things/'+data+'">'+ data+'</a>' : data;
				},
				userlink: function userlink(data, type, full, meta) {
					return type==='display' && !!data ? '<a about="_blank" href="/Thingworx/Users/'+data+'">'+ data+'</a>' : data;
				},
				ellipsis: function ellipsis(data, type, full, meta) {
					return type==='display' && !!data && data.length>30 ?
						'<span title="'+data+'">'+data.substr(0, 27)+'...</span>' :
						data;
				},
				location: function location(data, type, full, meta) {
					return type==='display'  && !!data ? data.latitude +':'+ data.longitude : data;
				},
				array: 'array[, ]',
				isodate: function isodate(data, type, full, meta) {
					return type==='display'  && !!data ? moment(data).format('YYYY-MM-DD') : data;
				},
				isodatetime: function isodatetime(data, type, full, meta) {
					return type==='display'  && !!data ? moment(data).format('YYYY-MM-DD HH:mm:ss') : data;
				},
				json: function json(data, type, full, meta) {
					return type==='display' ? JSON.stringify(data) : data;
				},
				boolean: function boolean(data, type, full, meta) {
					if(type==='display') {
						var value = false;
						try {
							var parsed = JSON.parse(data);
							value = (typeof parsed==='boolean') ? parsed : data;
						}
						catch(e) {
							value = !!data;
						}
						return value ? '<input type="checkbox" checked="checked" readonly="readonly"/>' : '<input type="checkbox" readonly="readonly"/>';
					}
					return data;
				},
				html: function html(data, type, full, meta) {
					return data;
				}
			};
		}
		
		return defaultRenderers;
	};
	
	/**
	 * @return string the datatable base type
	 */
	this._baseType2Type = function (baseType) {
		switch(baseType) {
    		case "HTML":
        	case "HYPERLINK":
        	case "IMAGE":
        	case "IMAGELINK":
        	case "GROUPNAME":
        	case "MASHUPNAME":
        	case "THINGNAME":
        	case "USERNAME":
        		return 'html';
        	
        	case "DATETIME":
        		return 'date';
        	
        	case "INTEGER":
        	case "NUMBER":
        		return 'num';

        	case "BOOLEAN":
        		return 'boolean';

        	case "JSON":
        	case "LOCATION":
        	case "MENUNAME":
        	case "NOTHING":
        	case "STRING":
        	case "QUERY":
        	case "TEXT":
        	case "XML":
        		return 'string';
        		
        	default:
        		return 'string';
    	}
	};
	
	/**
	 * Helper to get the jeditable type
	 * @return the jeditable type from column type
	 */
	this.baseType2EditType = function(baseType) {
		switch(baseType) {
    	case "HYPERLINK":
    	case "IMAGE":
    	case "IMAGELINK":
    	case "GROUPNAME":
    	case "MASHUPNAME":
    	case "THINGNAME":
    	case "USERNAME":
    		return 'text';
    	
    	case "DATETIME":
    		return 'datepicker';
    	
    	case "INTEGER":
    	case "NUMBER":
    		return 'text';

    	case "BOOLEAN":
    		return 'checkbox';
    		
    	case "LOCATION":
    	case "MENUNAME":
    	case "NOTHING":
    	case "STRING":
    	case "QUERY":
    	case "TEXT":
    	case "XML":
    		return 'text';

		case "HTML":
		case "JSON":
			return 'textarea';
    		
    	default:
    		return 'text';
	}
	};
	
	/**
	 * Parse the ThingWorx message response on grid update
	 * @return the error message as string if an error append, "OK" otherwise
	 */
	this.parseUpdateResult = function(data) {
		var result;
		if(!!data && !!data.rows && data.rows.length>0) {
			result = data.rows[0]._0;
		}
		
		return !!result ? result : "OK";
	};
	
	/**
	 * Helper to load the right defined jeditable data. It search parameter from glovalData parameter, if not in columnDefinition
	 * Only used for select actually
	 * @param [object] editorColumnParam Hash of data objects indexed by column names
	 * @param [object] cDef the column definition object for the current column
	 * @param mixed value The string value of the cell
	 * @param string objectPath path of data in the object (optional)
	 * @return the jeditable data parameter or null
	 */
	this.getEditData = function(editorColumnParam, cDef, value, objectPath) {
		if(!!objectPath) {
			if(typeof value==='object') {
				value = this.getObjectValue(value, objectPath);
			}
			else {
				value = this.getObjectValue(JSON.parse(value), objectPath);
			}
		}
		var data = value;
		
		if('select'===cDef.editType) {
			
			/* read data from globalData then columnDefintion */
			if(!!editorColumnParam && !!editorColumnParam[cDef.data]) {
				data = editorColumnParam[cDef.data];
			}
			else if(!!cDef.editParam) {
				data = cDef.editParam;
			}
			
			/* select the right element if data defined */
			if(!!data && data[value]) {
				data.selected = value;
			}
		}
		return data;
	};
    
	/**
	 * @return function the renderer function from given renderers object
	 */
    this._baseType2Render = function(baseType, renderers) {
    	switch(baseType) {
			case "HYPERLINK":
				return renderers.hyperlink;
			case "IMAGE":
	    	case "IMAGELINK":
	    		return renderers.imagelink;
	    	case "GROUPNAME":
	    		return renderers.grouplink;
	    	case "MASHUPNAME":
	    		return renderers.mashuplink;
	    	case "MENUNAME":
	    		return renderers.menulink;
	    	case "THINGNAME":
	    		return renderers.thinglink;
	    	case "USERNAME":
	    		return renderers.userlink;
	    	case "DATETIME":
	    		return renderers.isodate;
	    	case "JSON":
	    		return renderers.json;
	    	case "LOCATION":
	    		return renderers.location;
	    		
			case "HTML":
	    	case "INTEGER":
	    	case "NUMBER":
	    	case "BOOLEAN":
	    	case "NOTHING":
	    	case "STRING":
	    	case "QUERY":
	    	case "TEXT":
	    	case "XML":
	    		return renderers.text;
	    	default:
	    		return renderers.text;
		}
    };
    
    /**
     * Create basic column property from datashape
     * @return [array] of column definitions
     */
    this.dataShapeToColumnFormat = function(infoTableDataShape) {
    	
    	var renderers = this.getDefaultRenderers();
    	
    	var sortedDataShape = _.sortBy(infoTableDataShape, function(val, key, obj){
            return (val.ordinal || 0);
        });
        
        var columnFormat = [];
        
        _.each(sortedDataShape, function(fieldDef, key, list){
        	columnFormat.push({
        		data: fieldDef.name,
        		title: fieldDef.name,
        		defaultContent: '',
        		width: undefined,
        		className: 'dt-center',
        		orderable: true,
        		searchable: true,
        		issearchselect: false,
        		type: self._baseType2Type(fieldDef.baseType),
        		baseType: fieldDef.baseType,
        		clickable: false,
        		editable: false,
        		editType: undefined,
        		editParam: undefined,
        		editData: undefined,
        		render: self._baseType2Render(fieldDef.baseType, renderers)
        	});
        });
        
        return columnFormat;
    };
    
    /**
     * Create column definition from data
     * @return [array] of column definitions
     */
    this.dataToColumnFormat = function(row) {
    	var columnFormat = [];
    	
    	 for(var i in row) {
    		 columnFormat.push({
         		data: i,
         		title: i,
         		defaultContent: '',
         		width: undefined,
         		className: 'dt-center',
         		orderable: true,
         		searchable: true,
         		issearchselect: false,
         		type: 'string',
         		baseType: 'STRING',
         		clickable: false,
         		editable: false,
         		editType: undefined,
        		editParam: undefined,
        		editData: undefined,
        		render: undefined
         	});
    	 }
    	 
    	 return columnFormat;
    };
    
    /**
     * Try to parse a function from input
     * Can raise exception from eval function
     * @return function or undefined
     */
    this.parseFunctionFromString = function(s) {
    	if(!s || !s.trim()) {
    		return undefined;
    	}
    	
    	var fn;
    	try {
    		/* jshint evil: true */
    		fn = eval(s);
    	}
    	catch(e) {
    		try {
    			fn = eval('(' + s + ')');
    		}
    		catch(e2) {
    			fn = s;
    		}
    	}
    	return fn;
    };
    
    /**
     * Function to reviver function from strings for JSON.parse
     * Very ugly but no work so bad
     */
    this.parseReviver = function(index, value) {
    	/* jshint evil: true */
    	if(index==='render' && typeof value==='string') {
    		var potentialFn = self.getDefaultRenderers()[value];
    		if(!!potentialFn) {
    			return potentialFn;
    		}
    		
    		return self.parseFunctionFromString(value);
    	}
    	
    	else if(index==='exportFormatter' && typeof value==='string') {
    		return self.parseFunctionFromString(value);
    	}
    	
    	return value;
    };
    
    /**
     * Function to stringify functions for JSON.stringify
     */
    this.stringifyReplacer = function(index, value) {
    	if(index==='render' && typeof value==='function') {
    		if(!!self.getDefaultRenderers()[value.name]) {
    			return value.name;
    		}
    		return String(value);
    	}
    	
    	else if(index==='exportFormatter' && typeof value==='function') {
    		return String(value);
    	}
    	
    	return value;
    };
    
    /**
     * Add clickable class for clickable cells
     */
    this.addCreatedCellCallback = function(colDefinitions) {
    	return colDefinitions.map(function(cDef) {
			cDef.createdCell = function(td, cellData, rowData, row, col) {
				var $td = $(td);
				if(!!cDef.editable) {
					$td.addClass('editable');
				}
				
				//add clickable only if non editable
				else if(true===cDef.clickable) {
					$td.addClass('clickable');
				}
			};
    		return cDef;
    	});
    };
    
    
    /** Helper to get data from string path into object */
    this.setObjectValue = function(object, path, value) {
        var a = path.split('.');
        var o = object;
        for (var i = 0; i < a.length - 1; i++) {
            var n = a[i];
            if (n in o) {
                o = o[n];
            } else {
                o[n] = {};
                o = o[n];
            }
        }
        o[a[a.length - 1]] = value;
    };

    /** Helper to set data from string path into object */
    this.getObjectValue = function(object, path) {
        var o = object;
        path = path.replace(/\[(\w+)\]/g, '.$1');
        path = path.replace(/^\./, '');
        var a = path.split('.');
        while (a.length) {
            var n = a.shift();
            if (n in o) {
                o = o[n];
            } else {
                return;
            }
        }
        return o;
    };
};