TW.Runtime.Widgets.enhancedgrid = function () {
	var self = this;
	this.dataTable = null;
	this.renderers = null;
	this.needColumnDefRefresh = true;
	this.columnDefinitions = null;
	this.gridLibrary = new TW.EnhancedGridLibrary();
	this.editorColumnParam = null;
	this.infoTableDataShape = null;

    this.runtimeProperties = function () {
    	return {
    		needsDataLoadingAndError: true,
    		supportsAutoResize: true
    	};
    };
	
    /**
     * Import required libraries and draw the HTML
     */
	this.renderHtml = function () {
		
		//test datatable loaded
		if(typeof $().dataTable === 'undefined') {
			$("head").append('<link type="text/css" rel="stylesheet" href="../Common/extensions/EnhancedGrid/ui/enhancedgrid/lib/DataTables-1.10.15/datatables.min.css" />');
			$("head").append('<link type="text/css" rel="stylesheet" href="../Common/extensions/EnhancedGrid/ui/enhancedgrid/lib/ToolTipster-4.2.5/css/tooltipster.bundle.min.css" />');
			
			$("head").append('<script src="../Common/extensions/EnhancedGrid/ui/enhancedgrid/lib/DataTables-1.10.15/datatables.min.js"></script>');
			$("head").append('<script src="../Common/extensions/EnhancedGrid/ui/enhancedgrid/lib/ToolTipster-4.2.5/js/tooltipster.bundle.min.js"></script>');
			$("head").append('<script src="../Common/extensions/EnhancedGrid/ui/enhancedgrid/lib/jquery-jeditable-1.7.3/jquery.jeditable.js"></script>');
			$("head").append('<script src="../Common/extensions/EnhancedGrid/ui/enhancedgrid/lib/jquery.jeditable.datepicker.js"></script>');
			$("head").append('<script src="../Common/extensions/EnhancedGrid/ui/enhancedgrid/lib/jquery.jeditable.datetimepicker.js"></script>');
			$("head").append('<script src="../Common/extensions/EnhancedGrid/ui/enhancedgrid/lib/jquery.jeditable.checkbox.js"></script>');
			
		}
		
		var css = '';
		if(!!this.getProperty('CustomCss')) {
			var style = $('<style scoped="scoped"></style>');
			style.text(this.getProperty('CustomCss'));
			css = style[0].outerHTML;
		}
		
		return '<div class="widget-content widget-enhancedgrid">' +
			css +
			'<table class="' + this.getProperty('DefaultStylingOptions') + '" cellspacing="0" width="100%"><tbody/></table>' +
		'</div>';
	};
	
	/**
	 * Initiate the datatable with empty data
	 */
	this.afterRender = function () {
		this._refreshColumnDefinitions();
		this._initDataTable([]);
		
		//try to load editor column parameters
		var ecp = this.getProperty('EditorColumnParameter');
		if(!!ecp) {
			try {
				this.editorColumnParam = JSON.parse(ecp);
			}
			catch(e) {
				TW.log.error('Cannot parse EditorColumnParameter value: ' + e);
			}
		}
	};
	
	this.updateProperty = function (updatePropertyInfo) {
		var property = this._getSingleProperty(updatePropertyInfo);
		
		if(property !== undefined) {
			
			/* All data updated */
			if ('Data' === updatePropertyInfo.TargetProperty) {
				this._destroyDataTable();
				this.clearEditedData();
				this.infoTableDataShape = updatePropertyInfo.DataShape;
				
				if(this.needColumnDefRefresh) {
					this._refreshColumnDefinitions(this.infoTableDataShape);
				}
				
				this._initDataTable(property);
			}
			
			/* Column definition changed */
			else if('ColumnDefinition' === updatePropertyInfo.TargetProperty) {
				this._destroyDataTable();
				this.clearEditedData();
				
				this.needColumnDefRefresh = true;
				this.setProperty('ColumnDefinition', property);
				this._refreshColumnDefinitions();
				
				this._initDataTable([]);
			}
			
			/* Hide some columns */
			else if('ColumnHidden' === updatePropertyInfo.TargetProperty) {
				this.needColumnDefRefresh = true;
				this.setProperty('ColumnHidden', property);
				this.updateColumnVisibility();
			}
			
			/* Some data updated, be careful can unsync main data select */
			else if('UpdateData' === updatePropertyInfo.TargetProperty) {
				var keyColumn = this.getProperty('PrimaryKey');
				
				if(!keyColumn) {
					TW.log.error('Cannot update data without Primary Key defined.');
				}
				else {
					
					for(var i=0; i<property.length; i++) {
						var newRow = property[i];
						var primaryKey = newRow[keyColumn];
						
						if(!primaryKey) {
							TW.log.error('Primary key ' + keyColumn + ' is not defined for row ' + i);
						}
						else {
							var oldRow = this.dataTable.row('#' + primaryKey);
							if(oldRow.any()) {
								oldRow.data(newRow).invalidate();
							}
							else {
								this.dataTable.row.add(newRow);
							}
						}
					}
					
					/* do not trigger EditedData event, user know that because he called data update, and prevent cyclic updates */
					/* EditedData reserved for inline changes */
					this._populateSelectSearchOptions(this.dataTable);
					this.dataTable.rows({selected: true}).deselect();
					this.dataTable.draw(false);
					this.updateEditedData();
				}
			}
			
			/* Some EditorColumnParameter shall be updated*/
			else if ('EditorColumnParameter' === updatePropertyInfo.TargetProperty) {
				try {
					this.editorColumnParam = JSON.parse(property);
					//require redraw to apply onDraw that contain
					this.dataTable.rows().draw(false);
				}
				catch(e) {
					TW.log.error('Cannot parse EditorColumnParameter value: ' + e);
				}
			}
			
			/* a complex header has been defined */
			else if('ComplexHeader' === updatePropertyInfo.TargetProperty) {
				this._destroyDataTable();
				this.clearEditedData();
				
				this.setProperty('ComplexHeader', property);
				
				this._initDataTable([]);
			}
			
			
		}
	};
	
	/**
	 * Hack to be able to get a single value from a mapped infotable or direct service result
	 * @param [object]
	 * @return the value mapped, all rows if it's an infotable, undefined otherwise
	 */
	this._getSingleProperty = function(updatePropertyInfo) {
		if(!!updatePropertyInfo.ActualDataRows && updatePropertyInfo.ActualDataRows.length==1 && !!updatePropertyInfo.SourceProperty) {
			return updatePropertyInfo.ActualDataRows[0][updatePropertyInfo.SourceProperty];
		}
		else if (!!updatePropertyInfo.ActualDataRows) {
			return updatePropertyInfo.ActualDataRows;
		}
		else {
			return undefined;
		}
	};
	
	this._initDataTable = function(data) {
		var $table = this.jqElement.find('table');
		var complexHeader = this.getProperty('ComplexHeader');
		var scrollYValue = this.getProperty('FixedHeader')===true
			? this.jqElement.height()-80 + "px"
			: undefined;
		
		if(!!complexHeader) {
			$table.prepend(complexHeader);
		}
		
		this.dataTable = $table.DataTable({
			data:				data,
			columns:			this.columnDefinitions,
			deferRender:		this.getProperty('DeferRender')===true,
			info:				this.getProperty('DisplayInfo')===true,
			paging:				this.getProperty('Paging')===true,
			pageLength:			this.getProperty('PageLength'),
			lengthMenu: 		[[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "All"]],
			ordering:			this.getProperty('Ordering')===true,
			searching:			this.getProperty('Searching')===true,
			scrollY:			scrollYValue,
			scrollX:			true,
			fixedColumns:		this._getFixedColumns(),
			select:				this._getSelectProperty(),
			stateSave:			this.getProperty('StateSave')===true,
			stateDuration:		this.getProperty('StateDuration'),
			rowId: 				this.getProperty('PrimaryKey'),
			rowCallback:		this._onRowDrawn,
			dom: 'lfrtip',
			buttons:			[
				{
					extend: 'csv', name: 'csv',
					exportOptions: {columns: ':visible', format: {body: self.exportFormatter}}
				},
				{
					extend: 'excel', name: 'excel',
					exportOptions: {columns: ':visible', format: {body: self.exportFormatter}}
				}
			],
			initComplete: 		function() {
				self._createSearchInputs(this.api());
				self._populateSelectSearchOptions(this.api());
				self._enableSelection(this.api());
				self._enableClicks(this.api());
				self._enableSearch(this.api());
				self._listenButtonEvents(this.api());
			}
		});

		this.resize(this.jqElement.width(), this.jqElement.height());
	};
	
	/**
	 * Callback on row drawn activate the editor if needed
	 */
	this._onRowDrawn = function(rowNode, data, index) {
		
		var dApi = this.api();
		var columns = dApi.init().columns;
		var $tr = $(rowNode);
		var row = dApi.row($tr);
		var $cells;
		
		
		
		if(true===self.getProperty('IsEditable')) {
			
			var keyColumn = self.getProperty('PrimaryKey');
			var thingname = self.getProperty('UpdateThing');
			var servicename = self.getProperty('UpdateService');
			var isLocalEdit = !thingname || !servicename;
		
			if(!keyColumn) {
				TW.log.error('Cannot edit data without Primary Key defined.');
				return;
			}
			
			$cells = $tr.find('>td.editable');
			var url = '/Thingworx/Things/'+thingname+'/Services/'+servicename+'?Accept=application%2Fjson-compressed&_twsr=1&Content-Type=application%2Fjson';
			var editorIndicator = '<img src="../Common/extensions/EnhancedGrid/ui/enhancedgrid/lib/jquery-jeditable-1.7.3/img/loading.gif">';
			
			$cells.each(function() {
				var $td = $(this);
				
				var cell = dApi.cell($td);
				var initialData = cell.data();
				var column = dApi.column(cell.index().column);
				var cDef = columns[column.index()];
				
				var editorType = !!cDef.editType ? cDef.editType : 'text';
				var editorData = self.gridLibrary.getEditData(self.editorColumnParam, cDef, cell.data(), cDef.editData);
				
				var submitFunction = function(value, settings) {
					var parsedData;
					
					/* update only data in original value using editData path */
					if(!!cDef.editData) {
						if(typeof initialData==='object') {
							parsedData = initialData;
							self.gridLibrary.setObjectValue(parsedData, cDef.editData, value);
							value = parsedData;
						}
						else {
							parsedData = JSON.parse(initialData);
							self.gridLibrary.setObjectValue(parsedData, cDef.editData, value);
							value = JSON.stringify(parsedData);
						}
					}
					
					if(isLocalEdit) {
						TW.log.info('Data will be persisted locally.');
						cell.data(value);
						self._populateSelectSearchOptions(self.dataTable, self.dataTable.column(cell.index().column));
						self.updateEditedData();
						
						self.dataTable.rows({selected: true}).deselect();
						self.dataTable.row(cell.index().row).select();
						
						self.jqElement.triggerHandler('DataEdited');
						cell.invalidate().draw(false);
					}
					else {
						TW.log.info('Call service ' + thingname + '.' + servicename + ' to persist changes.');
						
						$.ajax({
							type: 'POST',
							url: url,
							dataType: 'json',
							contentType: 'application/json',
							data: JSON.stringify({
								row: row.id(),
								column: column.dataSrc(),
								data: value
							}),
							success: function(data, textStatus, xhr) {
								var resultMessage = self.gridLibrary.parseUpdateResult(data);
								
								if("OK"===resultMessage) {
									cell.data(value);
									self._populateSelectSearchOptions(self.dataTable, self.dataTable.column(cell.index().column));
									self.updateEditedData();
									
									self.dataTable.rows({selected: true}).deselect();
									self.dataTable.row(cell.index().row).select();
									
									self.jqElement.triggerHandler('DataEdited');
								}
								else {
									TW.log.error('Update service error: ' + resultMessage);
									TW.Runtime.showStatusText('error', 'Update service error: ' + resultMessage);
								}
								
								cell.invalidate();
								self.dataTable.draw(false);
							},
							error: function(xhr, textStatus, errorThrown) {
								TW.log.error('Update service unknown error ' + xhr.status + ': ' + errorThrown);
								TW.Runtime.showStatusText('error', 'Update service unknown error ' + xhr.status + ': ' + errorThrown);
								
								cell.invalidate();
								self.dataTable.draw(false);
							}
						});
					}
					
					return value;
				};
				
				/* init the editor */
				$td.editable('destroy');
				$td.editable(submitFunction, {
					type: editorType,
					data: JSON.stringify(editorData),
					indicator: editorIndicator,
					placeholder: ''
				});
			});
		}
		
		
		/* add tooltip on cells */
		$cells = $tr.find('>td');
		$cells.each(function() {
			var $td = $(this);
			var $tooltip = $td.find('.tooltip-content');
			if($tooltip.length>0) {
				if($td.hasClass('tooltipstered')) {
					$td.tooltipster('content', $tooltip);
				}
				else {
					$td.tooltipster({content: $tooltip});
				}
			}
			else {
				if($td.hasClass('tooltipstered')) {
					$td.tooltipster('destroy');
				}
			}
		});
	};
	
	/**
	 * Hide or display columns from ColumnHidden property
	 */
	this.updateColumnVisibility = function() {
		var hiddenColumns = this.getProperty('ColumnHidden') || [];
		var hiddenColumnsHash = {};
		var needRedraw = false;
		
		//array to hash
		$.each(hiddenColumns, function(i, value) {
			if(!!value.item) {
				hiddenColumnsHash[value.item] = true;
			}
		});
		
		this.dataTable.columns().every(function() {
			var column = this;
			
			if(column.visible()) {
				if(true===hiddenColumnsHash[column.dataSrc()]) {
					column.visible(false, false);
					needRedraw = true;
				}
			}
			else {
				if(undefined===hiddenColumnsHash[column.dataSrc()]){
					column.visible(true, false);
					needRedraw = true;
				}
			}
		});
		
		if(true===needRedraw) {
			this.dataTable.columns.adjust().draw(false);
		}
	};
	
	
	/**
	 * Compute the column definitions considering widget properties, or datashape or data
	 */
	this._refreshColumnDefinitions = function(infoTableDataShape, data) {
		
		//try to parse from widget property
		var cDef = this.getProperty('ColumnDefinition');
		if(!!cDef) {
			try {
				this.columnDefinitions = JSON.parse(cDef, this.gridLibrary.parseReviver);
				this.columnDefinitions = this.gridLibrary.addCreatedCellCallback(this.columnDefinitions);
				this.needColumnDefRefresh = false;
			}
			catch(error) {
				TW.log.error('Invalid column definitions "' + idx + '"', error);
			}
		}
		
		// try to create from datashape
		else if(!!infoTableDataShape) {
			this.columnDefinitions = this.gridLibrary.dataShapeToColumnFormat(infoTableDataShape);
			this.needColumnDefRefresh = true;
		}
		
		// a complex header exists, datatable will be able to generate basic column definition
		else if(!!this.getProperty('ComplexHeader')) {
			this.columnDefinitions = null;
			this.needColumnDefRefresh = true;
		}
		
		//no datashape but data
		else if(!!data && datalength>0) {
			this.columnDefinitions = this.gridLibrary.dataToColumnFormat(data[0]);
			this.needColumnDefRefresh = true;
		}
		
		//No column definition can be inferred or generated, return a basic one that could be inferred from data later
		else {
			this.columnDefinitions = [{
				data: 'nodata',
	    		title: 'Must be bound to data',
	    		defaultContent: '',
	    		orderable: false,
	    		searchable: false,
	    		type: 'string'
			}];
			this.needColumnDefRefresh = true;
		}
		
		/* update column definition from hidden columns if defined */
		var hiddenColumns = this.getProperty('ColumnHidden');
		if(!!hiddenColumns) {
			var hiddenColumnsHash = {};
			
			//array to hash
			$.each(hiddenColumns, function(i, value) {
				if(!!value.item) {
					hiddenColumnsHash[value.item] = true;
				}
			});
			
			$.each(this.columnDefinitions, function(i, value) {
				if(true===hiddenColumnsHash[value.data]) {
					self.columnDefinitions[i].visible = false;
				}
			});
		}
	};
	
	/**
	 * @return [mixed] true/false if bool or empty value, the parsed FixedColumn object otherwise
	 */
	this._getFixedColumns = function() {
		var property = this.getProperty('FixedColumns');
		return !!property ? JSON.parse(property) : false;
	};
	
	/**
	 * @return [mixed] false if disabled, the select parameter from widget properties otherwise
	 */
	this._getSelectProperty = function() {
		if(!this.getProperty('EnableSelection')) {
			return false;
		}
		
		return {
			style: this.getProperty('SelectStyle'),
			selector: 'tr'
		};
	};
	
	/**
	 * Nothing to do yet but mandatory: https://community.thingworx.com/thread/39190
	 */
	this.handleSelectionUpdate = function (propertyName, selectedRows, selectedRowIndices) {
		TW.log.debug('Handle selection update: ' + propertyName);
	};
	
	/**
	 * Create the search inputs if necessary
	 * @param the dataTable API object
	 */
	this._createSearchInputs = function(dtApi) {
		if(self.getProperty('Searching')===true && self.getProperty('IndividualColumnSearching')===true) {
			
			var columns = dtApi.init().columns;
			
			dtApi.columns().every(function() {
				var column = this;
				var cDef = columns[column.index()];
				
				if(true===cDef.searchable) {
					var $input;
					
					if(cDef.issearchselect===true) {
						$input = $('<select data-index="' + column.index() + '"><option value=""></option></select>');
					}
					
					else {
						$input = $('<input data-index="' + column.index() + '" type="text" placeholder="Search ' + cDef.title + '" />');
					}
					
					// prevent header sort on click search
					$input.on('click', function(e) {
						e.stopImmediatePropagation();
					});
					
					$('<div/>').append($input).appendTo(column.header());
					
				}
			});
		}
	};

	/**
	 * Update select search options for select inputs if search activated
	 * @param [object] the datatable API object
	 * @param [object] column The column update that need update (update all columns if not defined)
	 */
	this._populateSelectSearchOptions = function(dtApi, column) {
		if(self.getProperty('IndividualColumnSearching')===true) {
			
			var columns = dtApi.init().columns;
			var dtColumns = !!column ? column : dtApi.columns();
			
			dtColumns.every(function() {
				var column = this;
				var cDef = columns[column.index()];
				
				if(true===cDef.searchable && true===cDef.issearchselect) {
					
					var $select = $(column.header()).find('select');
					$select.empty();
					$select.append('<option value=""></option>');
					
					dtApi.cells('', column.index())
						.render('filter')
						.filter(function(value){return !!value;})
						.unique()
						.sort()
						.each(function(d, j) {
							$select.append('<option value="'+d+'">'+d+'</option>');
						})
					;
				}
			});
		}
	};
	
	/**
	 * Enable Cell clicks and row double click events
	 * @param the datatable API object
	 */
	this._enableClicks = function(dtApi) {
		var $tbody = $(dtApi.tables().body());
		
		$tbody.on('dblclick', '>tr', self.rowDblClickHandler);
		$tbody.on('click', '>tr >td.clickable', self.cellClickHandler);
	};
	
	/**
	 * Enable table selection if defined
	 * @param the datatable API object
	 */
	this._enableSelection = function(dtApi) {
		dtApi.on('deselect select', function(e, dt, type, indexes) {
			self.updateSelection('Data', dt.rows({selected:true}).indexes().toArray());
		});
	};
	
	this._enableSearch = function(dtApi) {
		$container = $(dtApi.table().container());
		
		$container.on('change', 'thead select', function() {
			var column = dtApi.column($(this).data('index'));
			column.search(this.value, false, false).draw();
		});
		
		$container.on('keyup change', 'thead input', function() {
			var column = dtApi.column($(this).data('index'));
			if(column.search()!==this.value) {
				column.search(this.value).draw();
			}
		});
	};
	
	this._listenButtonEvents = function(dtApi) {
		/* can't have the export done event. Code below don't work */
		/*dtApi.on('buttons-action', function(e, buttonApi, dataTable, node, config ) {
			self.jqElement.triggerHandler('ExportEnded');
		});*/
	};
	
	this._destroyDataTable = function() {
		if(!!this.dataTable) {
			var $table = $(this.dataTable.table().node());
			this.dataTable.destroy();
			$table.empty();
			$table.append('<tbody/>');
			this.dataTable = null;
		}
		
		//clean if something wrong
		else {
			this.jqElement.empty();
			var css = this.getProperty('CustomCss');
			if(!!css) {
				var style = $('<style scoped="scoped"></style>').text(css);
				this.jqElement.append(style);
			}
			this.jqElement.append('<table class="' + this.getProperty('DefaultStylingOptions') + '" cellspacing="0" width="100%"><tbody/></table>');
		}
	};
	
	/**
	 * Destroy Jquery event bindings and call datatable destructor
	 */
	this.beforeDestroy = function() {
		this._destroyDataTable();
		this.renderers = null;
		this.columnDefinitions = null;
		this.clearEditedData();
	};
	
	this.resize = function (width, height) {
		if(this.getProperty('FixedHeader')===true) {
			
			var yOffset = this._computeYOffset();
			var headerHeight = this.jqElement.find('.dataTables_scrollHead').outerHeight();
			
			this.jqElement.find('.dataTables_scrollBody').height(height - yOffset - headerHeight);
			this.dataTable.draw(false);
		}
		else {
			this.dataTable.columns.adjust().draw(false);
		}
	};

	/**
	 * Compute the scrollY size in pixels
	 * @return int ScrollY value depending on available space, header height, and options
	 */
	this._computeYOffset = function() {
		var wrapper = this.jqElement.find('.dataTables_wrapper');
		var childrens = wrapper.children();
		
		var isHeader = true;
		var preOffset = 0;
		var postOffset = 0;
		
		childrens.each(function() {
			var $e = $(this);
			if(isHeader===true && ($e.hasClass('dataTables_scroll') || $e.hasClass('DTFC_ScrollWrapper'))) {
				isHeader = false;
			}
			else {
				if(isHeader===true) {
					preOffset = Math.max(preOffset, $e.outerHeight());
				}
				else {
					postOffset = Math.max(postOffset, $e.outerHeight());
				}
			}
		});
		
		return 2 + preOffset + postOffset;
	};
	
	/**
	 * Callback when a service is invoked
	 */
	this.serviceInvoked = function(serviceName) {
		switch(serviceName) {
			case "ResetFilters":
				this.jqElement.find('div.dataTables_filter input').val('');
				
				//clear global search
				$(this.dataTable.table().header()).find('input,select').val('');
				
				//clear columns search
				this.dataTable.columns().every(function() {
					this.search('');
				});
				//search and re draw
				this.dataTable.search('').draw();
				break;
			
			case "CsvExport":
				this.dataTable.button('csv:name').trigger();
				break;
			
			case "ExcelExport":
				this.dataTable.button('excel:name').trigger();
				break;
				
			case "RemoveSelected":
				this.dataTable.rows({selected: true}).remove().draw();
				this.updateEditedData();
				self.jqElement.triggerHandler('DataEdited');
				break;
			
			default:
				TW.log.warn('Unkwnon EnhancedGrid service ' + serviceName);
				break;
		}
	};
	
	/**
	 * Callback to format columns at export
	 */
	this.exportFormatter = function(innerHTML, row, col, node) {
		var columns = self.dataTable.init().columns;
		var column = self.dataTable.column(col + ':visible');
		var cDef = columns[column.index()];
		
		if(!!cDef && !!cDef.exportFormatter) {
			return cDef.exportFormatter.call(self.dataTable, innerHTML, row, col, node);
		}
		
		return innerHTML;
	};
	
	/**
	 * Helper to persist data on EditedGrid if enabled
	 */
	this.updateEditedData = function() {
		if(true===this.getProperty('IsEditable')) {
			var editedTable = TW.InfoTableUtilities.CloneInfoTable({
				"dataShape" : { "fieldDefinitions" : this.infoTableDataShape},
				"rows" : this.dataTable.data().toArray()
			});
			this.setProperty('EditedData',editedTable);
		}
	};
	
	/**
	 * Helper to clear edited data
	 */
	this.clearEditedData = function() {
		if(true===this.getProperty('IsEditable')) {
			this.setProperty('EditedData', {
				dataShape: {fieldDefinitions: {}},
				rows: []
			});
		}
	};
	
	
	
	
	/** ***************************
	 * Jquery handlers for clicks
	 * Be careful this keyword refer to the jquery element here not the widget
	 ***************************** */
	
	/**
	 * Handler for row when double clicked
	 */
	this.rowDblClickHandler = function(e) {
		e.stopImmediatePropagation();
		
		var row = self.dataTable.row(this);
		
		self.dataTable.rows({selected: true}).deselect();
		row.select();
		
		self.jqElement.triggerHandler('RowDblClicked');
	};
	
	/**
	 * Handler for cells when clicked
	 */
	this.cellClickHandler = function(e) {
		
		//prevent execution when user have CTRL key active
		if(!e.ctrlKey && !e.shiftKey) {
			e.stopImmediatePropagation();
			
			var cell = self.dataTable.cell(this);
			var row = self.dataTable.row(cell.index().row);
			var column = self.dataTable.column(cell.index().column);
			
			self.dataTable.rows({selected: true}).deselect();
			row.select();
			
			self.setProperty('ClickedColumnName', column.dataSrc());
			self.jqElement.triggerHandler('CellClicked');
		}
	};
	
};